<?

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\User;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h3>Страница пользователя <?= $model->username?></h3>

    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'Username',
                    'format' => 'raw',
                    'value' => function ($model)
                    {
                        $user = $model->getUser()->one();
                        return Html::a($user->username, Url::toRoute(['view', 'id' => $user->id]));
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{accept}',
                    'buttons' => [
                        'accept' => function($url, $model) {return Html::a('Принять приглашение', ['accept', 'id' => $model->user_id]);}
                    ]
                ],
            ],
        ]); ?>

</div>