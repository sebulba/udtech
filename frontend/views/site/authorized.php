<?

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'List';
?>
<div class="site-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {friendship}',
                'buttons' => [
                    'friendship' => function($url, $model, $id) use ($friendModel)
                    {
                        foreach ($friendModel as $item)
                        {
                            if($item->user_id == Yii::$app->user->id && $item->friend_id == $id)
                                if($item->status)
                                    $result = [
                                        'text' => 'удалить из друзей',
                                        'link' => Url::toRoute([
                                            'remove-from-friends',
                                            'id' => $id
                                        ])
                                    ];
                                else
                                    $result = [
                                        'text' => 'отменить заявку',
                                        'link' => Url::toRoute([
                                            'cancel',
                                            'id' => $id
                                        ])
                                    ];
                            elseif ($item->friend_id == Yii::$app->user->id && $item->user_id == $id)
                                if($item->status)
                                    $result = [
                                        'text' => 'удалить из друзей',
                                        'link' => Url::toRoute([
                                            'remove-from-friends',
                                            'id' => $id
                                        ])
                                    ];
                                else
                                    $result = [
                                        'text' => 'принять приглашение',
                                        'link' => Url::toRoute([
                                            'accept',
                                            'id' => $id
                                        ])
                                    ];
                        }

                        if(!isset($result))
                            $result = [
                                'text' => 'добавить в друзья',
                                'link' => Url::toRoute([
                                    'add',
                                    'id' => $id
                                ])
                            ];

                        return Html::a($result['text'], $result['link']);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
