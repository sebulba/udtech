<?

use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <h3>Для авторизации воспользуйтесь одним из аккаунтов указанных ниже или зарегистрируйтесь самостоятельно</h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'username',

            [
                'label' => 'password',
                'value' => function($data) {return $data->username.'pass';}
            ],
        ],
    ]); ?>
</div>