<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

use common\models\LoginForm;
use common\models\User;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\UserRelation;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(
                Yii::$app->user->isGuest
                    ? null
                    : ['<>', 'id', Yii::$app->user->id]
            ),
        ]);

        if(Yii::$app->user->isGuest)
        {
            return $this->render('guest',[
                'dataProvider' => $dataProvider
            ]);
        }
        else
        {
            $friendModel = UserRelation::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->orWhere(['friend_id' => Yii::$app->user->id])
                ->all();

            return $this->render('authorized',[
                'dataProvider' => $dataProvider,
                'friendModel' => $friendModel
            ]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionRemoveFromFriends($id)
    {
        UserRelation::find()
            ->andFilterWhere([
                'and',
                ['user_id' => Yii::$app->user->id],
                ['friend_id' => $id]
            ])
            ->orFilterWhere([
                'and',
                ['user_id' => $id],
                ['friend_id' => Yii::$app->user->id]
            ])
            ->one()
            ->delete();

        return $this->goHome();
    }

    public function actionCancel($id)
    {
        UserRelation::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['friend_id' => $id])
            ->one()
            ->delete();

        $this->goHome();
    }

    public function actionAdd($id)
    {
        $friendQuery = new UserRelation([
            'user_id' => Yii::$app->user->id,
            'friend_id' => $id
        ]);

        $friendQuery->save();

        $this->goHome();
    }

    public function actionAccept($id)
    {
        $friendQuery = UserRelation::find()->where([
            'user_id' => $id,
            'friend_id' => Yii::$app->user->id
        ])->one();

        $friendQuery->status = 1;
        $friendQuery->save();

        $this->redirect(Url::previous());
    }

    public function actionProfile()
    {
        Url::remember();
        $model = User::find()->where(['id' => Yii::$app->user->id])->one();
        $dataProvider = new ActiveDataProvider([
            'query' => UserRelation::find()
                ->filterWhere([
                    'and',
                    ['friend_id' => Yii::$app->user->id],
                    ['status' => 0]
                ])
        ]);

        return $this->render('profile', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param string $route
     */
    public function actionView($id)
    {
        $model = User::findOne($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionDelete($id)
    {
        User::findOne($id)->delete();

        $this->goHome();
    }
}
